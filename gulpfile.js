// // including plugins
// var gulp = require('gulp')
// var minifyCss = require("gulp-minify-css");
 
// // task
// gulp.task('minify-css', function (done) {
//     gulp.src('css/main.css') // path to your file
//     .pipe(minifyCss())
//     .pipe(gulp.dest('css/minified'));
//      done();
//       console.log("FEITO");
// });



var gulp = require('gulp');
var minify = require('gulp-minify-css');
var rename = require('gulp-rename');

gulp.task('minify-css', function () {
	//gulp.src('style.css') 
    gulp.src(['css/*.css', '!css/font-awesome.min.css'])
        .pipe(minify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('dist/css'));
    ;
});

gulp.task('default', ['minify'], function() {

});

